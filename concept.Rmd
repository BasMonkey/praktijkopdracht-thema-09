---
title: "Concept"
author: "Bas Kasemir"
date: "`r Sys.Date()`"
output:
  pdf_document:
    latex_engine: xelatex
    toc: false
    toc_depth: 3
    highlight: tango
mainfont: Lato-Light
sansfont: Lato-Regular
romanfont: Lato-Light
monofont: NotoMono-Regular.ttf
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# EDA
## Hoe is de data ingedeeld?
De afmetingen (rijen en kolommen):
``` {r data pander}
library(pander)
# Read the data file
dia_data <- read.csv('data/diabetic_data.csv', header = TRUE, sep = ",")
pander(dim(dia_data))
```
De soorten waarden in de kolommen:
``` {r }
str(dia_data)
```


De headers van de dataset:
``` {r headers van data}

# Define first ten columns and rows
first5rows <- 1:5
first4cols <- 3:10

# Print it with pander, so it looks nice
pander(dia_data[first5rows,first4cols], caption="Example of the dataframe that shows the first five rows and first ten columns of the dataframe")
```

De hoeveelheden van de verrschillende waarden in verschillende (meest belangrijke) kolommen
``` {r values of the columns etc}
par(mfcol=c(3,2))
plot(dia_data$race, xlab="Ras", ylab="Aantal keer voorkomend", main="Verdeling van rassen")
plot(dia_data$gender, xlab="Geslacht", ylab="Aantal keer voorkomend", main="Verdeling van gelsacht")
plot(dia_data$age, xlab="Leeftijd", ylab="Aantal keer voorkomend", main="Verdeling van leeftijdsgroepen")

plot(dia_data$weight, xlab="Gewicht", ylab="Aantal keer voorkomend", main="Verdeling van Gewicht")

plot(dia_data$insulin, xlab="Insuline", ylab="Aantal keer voorkomend", main="Verdeling van insuline")

plot(dia_data$readmitted, xlab="Readmitted", ylab="Aantal keer voorkomend", main="Verdeling van terugkeer in dagen")
```

## Cleaning data
De kolommen weight, payercode, nateglinide, chlorpropamide, acetohexamide, tolbutamide zijn verwijderd, omdat deze niet significant genoeg waren volgens plots die zijn gemaakt. (Wegens ruimtegebrek zijn deze achterwege gelaten in dit concept. Dit zelfde geld voor de crosstables die zijn gemaakt bij de EDA.)
```{r cleanup data}
# Define the col numbers
# weight, payercode, nateglinide, chlorpropamide, acetohexamide, tolbutamide.
col_weight <- 6
col_payercode <- 11
col_nateglinide <- 27
col_chlorpropamide <- 28
col_acetohexamide <- 30
col_tolbutamide <- 33

cleaned_data <- dia_data[, -c(col_weight,col_payercode,col_nateglinide,col_chlorpropamide,col_acetohexamide,col_tolbutamide)]
cleaned_data[cleaned_data == "?"] <- NA

cleaned_data_without_NA <- cleaned_data[complete.cases(cleaned_data), ] # remove NA's from race column

```
## Weka experimenter resultaten
```
##Number_correct
Tester:     weka.experiment.PairedCorrectedTTester -G 4,5,6 -D 1 -R 2 -S 0.05 -result-matrix "weka.experiment.ResultMatrixPlainText -mean-prec 2 -stddev-prec 2 -col-name-width 0 -row-name-width 25 -mean-width 0 -stddev-width 0 -sig-width 0 -count-width 5 -print-col-names -print-row-names -enum-col-names"
Analysing:  Number_correct
Datasets:   1
Resultsets: 7
Confidence: 0.05 (two tailed)
Sorted by:  -
Date:       10/12/18 10:19 PM


Dataset                   (1) rules.Zero | (2) rules.O (3) bayes.N (4) functio (5) lazy.IB (6) trees.J (7) trees.R
------------------------------------------------------------------------------------------------------------------
R_data_frame             (100)   5486.40 |   5424.06 *   5711.66 v   5907.43 v   4732.10 *   5967.57 v   6089.59 v
------------------------------------------------------------------------------------------------------------------
                                 (v/ /*) |     (0/0/1)     (1/0/0)     (1/0/0)     (0/0/1)     (1/0/0)     (1/0/0)


Key:
(1) rules.ZeroR '' 48055541465867954
(2) rules.OneR '-B 6' -3459427003147861443
(3) bayes.NaiveBayes '' 5995231201785697655
(4) functions.SimpleLogistic '-I 0 -M 500 -H 50 -W 0.0' 7397710626304705059
(5) lazy.IBk '-K 1 -W 0 -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"' -3080186098777067172
(6) trees.J48 '-C 0.25 -M 2' -217733168393644444
(7) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1' 1116839470751428698

## False positive rate
Tester:     weka.experiment.PairedCorrectedTTester -G 4,5,6 -D 1 -R 2 -S 0.05 -result-matrix "weka.experiment.ResultMatrixPlainText -mean-prec 2 -stddev-prec 2 -col-name-width 0 -row-name-width 25 -mean-width 4 -stddev-width 2 -sig-width 1 -count-width 5 -print-col-names -print-row-names -enum-col-names"
Analysing:  False_positive_rate
Datasets:   1
Resultsets: 7
Confidence: 0.05 (two tailed)
Sorted by:  -
Date:       10/12/18 10:21 PM


Dataset                   (1) rules.Z | (2) rule (3) baye (4) func (5) lazy (6) tree (7) tree
---------------------------------------------------------------------------------------------
R_data_frame             (100)   0.00 |   0.01 v   0.03 v   0.00 v   0.11 v   0.01 v   0.00 v
---------------------------------------------------------------------------------------------
                              (v/ /*) |  (1/0/0)  (1/0/0)  (1/0/0)  (1/0/0)  (1/0/0)  (1/0/0)


Key:
(1) rules.ZeroR '' 48055541465867954
(2) rules.OneR '-B 6' -3459427003147861443
(3) bayes.NaiveBayes '' 5995231201785697655
(4) functions.SimpleLogistic '-I 0 -M 500 -H 50 -W 0.0' 7397710626304705059
(5) lazy.IBk '-K 1 -W 0 -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"' -3080186098777067172
(6) trees.J48 '-C 0.25 -M 2' -217733168393644444
(7) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1' 1116839470751428698

## Testingstijd
Tester:     weka.experiment.PairedCorrectedTTester -G 4,5,6 -D 1 -R 2 -S 0.05 -result-matrix "weka.experiment.ResultMatrixPlainText -mean-prec 2 -stddev-prec 2 -col-name-width 0 -row-name-width 25 -mean-width 3 -stddev-width 2 -sig-width 1 -count-width 5 -print-col-names -print-row-names -enum-col-names"
Analysing:  Elapsed_Time_testing
Datasets:   1
Resultsets: 7
Confidence: 0.05 (two tailed)
Sorted by:  -
Date:       10/12/18 10:49 PM


Dataset                   (1) rules.Z | (2) rule (3) baye (4) func (5) lazy.I (6) tree (7) trees
------------------------------------------------------------------------------------------------
R_data_frame             (100)   0.01 |   0.02 v   0.14 v   0.08 v   245.71 v   0.02 v   12.11 v
------------------------------------------------------------------------------------------------
                              (v/ /*) |  (1/0/0)  (1/0/0)  (1/0/0)    (1/0/0)  (1/0/0)   (1/0/0)


Key:
(1) rules.ZeroR '' 48055541465867954
(2) rules.OneR '-B 6' -3459427003147861443
(3) bayes.NaiveBayes '' 5995231201785697655
(4) functions.SimpleLogistic '-I 0 -M 500 -H 50 -W 0.0' 7397710626304705059
(5) lazy.IBk '-K 1 -W 0 -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"' -3080186098777067172
(6) trees.J48 '-C 0.25 -M 2' -217733168393644444
(7) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1' 1116839470751428698

## Trainingstijd
Tester:     weka.experiment.PairedCorrectedTTester -G 4,5,6 -D 1 -R 2 -S 0.05 -result-matrix "weka.experiment.ResultMatrixPlainText -mean-prec 2 -stddev-prec 2 -col-name-width 0 -row-name-width 25 -mean-width 2 -stddev-width 2 -sig-width 1 -count-width 5 -print-col-names -print-row-names -enum-col-names"
Analysing:  Elapsed_Time_training
Datasets:   1
Resultsets: 7
Confidence: 0.05 (two tailed)
Sorted by:  -
Date:       10/12/18 10:47 PM


Dataset                   (1) rules.Z | (2) rule (3) baye (4) functi (5) lazy (6) trees (7) trees.
--------------------------------------------------------------------------------------------------
R_data_frame             (100)   0.04 |   0.96 v   0.34 v   286.36 v   0.06 v   89.51 v   412.74 v
--------------------------------------------------------------------------------------------------
                              (v/ /*) |  (1/0/0)  (1/0/0)    (1/0/0)  (1/0/0)   (1/0/0)    (1/0/0)


Key:
(1) rules.ZeroR '' 48055541465867954
(2) rules.OneR '-B 6' -3459427003147861443
(3) bayes.NaiveBayes '' 5995231201785697655
(4) functions.SimpleLogistic '-I 0 -M 500 -H 50 -W 0.0' 7397710626304705059
(5) lazy.IBk '-K 1 -W 0 -A \"weka.core.neighboursearch.LinearNNSearch -A \\\"weka.core.EuclideanDistance -R first-last\\\"\"' -3080186098777067172
(6) trees.J48 '-C 0.25 -M 2' -217733168393644444
(7) trees.RandomForest '-P 100 -I 100 -num-slots 1 -K 0 -M 1.0 -V 0.001 -S 1' 1116839470751428698
```
## Conclusie
Na het bestuderen van de dataset bleek al snel dat er enkele kolommen zijn die niet relevant waren voor het onderzoek. De kolommen waren echter wel vrij "schoon". Voor de resulaten van de Weka experimenter geld dat er met zekerheid gezegd kan worden dat de juiste classifier gevonden is, namelijk andomForest. Dit is te beargumenteren met de volgende redenen. De score voor het aantal goed geclassificeerde instances is bij RandomForest het hoogst (6089.59), wat deze het meest betrouwbaar maakt. Daarnaast is de falspositive bij RandomForest, simpleLogistics en ZeroR 0.00. De testing tijd voor RandomForest is echter wel de een-na-hoogste waarde, met een gat tussen de lagere waardes. Deze waarde ligt wel dichter bij de lage waarden dan de hoogste uitschieter. Het maakt ook niet uit of de testing tijd hoog is, aangezien er niet binnen enkele seconde / minuten bepaald moet worden hoe groot de kans is dat een patient terug zal keren naar het ziekenhuis. De trainingstijd is voor RandomForest wel het hoogst. Dit maakt echter ook niet zo veel uit voor de onderzoeksvraag omdat het programma zeer waarschijnlijk in een ziekenhuis gebruikt zou kunnen worden. Een ziekenhuis heeft mogelijk de computers, of er toegang tot, om deze lang durende training makkelijk te laten uitvoeren. Het is ook niet zo dat dit model vaak vernieuwd zal moeten worden en dan binnen enkele minuten besichikbaar zou moeten zijn.

## Discussion
Voor een volgend ondrzoek in de toekomst zijn dit de aanbevelingen. In het originele onderzoek stond dat er rijen uit de dataset zijn verwijderd van patienten die zijn overleden of bijvoorbeeld in een verzorgingstehuis zijn gekomen. Dit had eigenlijk ook met de gebruikte dataset in de Weka Experimenter moeten gebeuren, maar dit was lastig uit te zoeken en dit las ik ook pas na het uitvoeren van de Experimenter. Dit zou namelijk de classifier nog accurater kunnen maken en de trainingstijd ook aanzienelijk kunnen verkorten.